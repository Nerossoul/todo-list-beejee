<?php
// index
Route::set("index.php", function () {
    IndexController::index();
});

Route::set("create", function () {
    IndexController::create();
});

Route::set("taskdone", function () {
    IndexController::updateTaskDone();
});

Route::set("taskdescription", function () {
    IndexController::updateTaskDescription();
});

//login
Route::set("login", function () {
    LoginController::index();
});

Route::set("login-check", function () {
    LoginController::login();
});

Route::set("logout", function () {
    LoginController::logout();
});