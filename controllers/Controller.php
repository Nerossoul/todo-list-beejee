<?php

class Controller extends DataBase
{
    public static function render($viewName, $viewData = array())
    {
        require_once "./views/" . $viewName . ".php";
    }
    public static function redirectTo($route)
    {
        header("Location: " . str_replace(Route::$currentRoute, $route, "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"));
    }
}