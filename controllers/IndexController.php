<?php
class IndexController extends Controller
{
    public static function index($orderBy = false, $desc = false, $page = 1)
    {
        $countTasks = self::query("SELECT COUNT(*) FROM `tasks`");

        $pagesCounter = ceil($countTasks[0][0] / PAGE_ITEMS_MAX);

        if (isset($_GET['page'])) {
            if ($_GET['page'] < $pagesCounter) {
                $page = (int) $_GET['page'];
            } else {
                $page = $pagesCounter;
            }
        }
        if (isset($_GET['desc'])) {
            $desc = $_GET['desc'];
        }
        if (isset($_GET['orderBy'])) {
            $orderBy = $_GET['orderBy'];
        }

        $sql = "SELECT * FROM `tasks`";
        if ($orderBy !== false) {
            $sortableCols = array("user_name", "e_mail", "done");
            if (in_array($orderBy, $sortableCols)) {
                $sql .= " ORDER BY " . $orderBy;
                if ($desc) {
                    $sql .= " DESC";
                }
            }
        }
        $sql .= " LIMIT " . PAGE_ITEMS_MAX;
        if ($page > 0) {
            $sql .= " OFFSET " . ($page - 1) * PAGE_ITEMS_MAX;
        }
        self::render("IndexView", array(
            "tasks" => self::query($sql),
            "page" => $page,
            "pages" => $pagesCounter,
            "desc" => !$desc,
            "orderBy" => $orderBy,
        )
        );

    }

    public static function create()
    {
        $page = 1;
        self::query('INSERT INTO tasks (user_name, e_mail, description, done, isAdminEdit) VALUES (?,?,?,?,?)', array(htmlspecialchars($_POST["user_name"]), $_POST["e_mail"], htmlspecialchars($_POST["description"]), 0, 0));
        setcookie("operation_result", "Задача успешно добавлена", time() + 3600, "/");
        self::redirectTo("");
    }

    public static function updateTaskDone()
    {
        if (Auth::isLogined()) {
            if (isset($_GET["id"])) {
                $sql = "UPDATE tasks SET done = " . $_GET["done"] . " WHERE id = " . $_GET["id"];
                self::query($sql);
                setcookie("operation_result", "сохранено", time() + 3600, "/");
            }
        } else {
            setcookie("operation_result", "Авторизуйтесь для редакторования задач", time() + 3600, "/");

            self::redirectTo("login");
        }

        self::redirectTo("");
    }

    public static function updateTaskDescription()
    {
        print_r($_POST);
        if (Auth::isLogined()) {
            if (isset($_POST["id"])) {
                $sql = "SELECT * FROM `tasks` WHERE id = " . $_POST["id"];
                $oldTaskData = self::query($sql);
                print_r($oldTaskData);

                if ($oldTaskData["description"] !== $_POST["description"]) {
                    $sql = "UPDATE tasks SET description =" . self::escape(htmlspecialchars($_POST["description"])) . ", isAdminEdit=1 WHERE id = " . $_POST["id"];
                    self::query($sql);
                    setcookie("operation_result", "сохранено", time() + 3600, "/");
                }
            }
            self::redirectTo("");
        } else {
            setcookie("operation_result", "Авторизуйтесь для редакторования задач", time() + 3600, "/");

            self::redirectTo("login");
        }
    }
}