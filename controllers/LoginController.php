<?php
class LoginController extends Controller
{
    public static function index()
    {
        self::render("LoginFormView");
    }

    public static function login()
    {
        if ($_POST["login"] === "admin" && $_POST["password"] === "123") {
            setcookie("auth_token", md5("admin123"), time() + 3600, "/", null, null, true);
            self::redirectTo("");
        } else {
            setcookie("operation_result", "Не верный логин или пароль", time() + 3600, "/");
            self::redirectTo("login");
        }
    }

    public static function logout()
    {
        setcookie("auth_token", "", time() - 3600, "/", null, null, true);
        self::redirectTo("login");
    }

}