<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>BeeJee Test</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/album/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">


    <style>
    .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    @media (min-width: 768px) {
        .bd-placeholder-img-lg {
            font-size: 3.5rem;
        }
    }
    </style>
</head>

<body>
    <header>
        <div class="collapse bg-dark" id="navbarHeader">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-md-7 py-4">
                        <h4 class="text-white">Текущая задача</h4>
                        <p class="text-muted">Необходимо создать приложение-задачник.
                            Задачи состоят из:<br>
                            - имени пользователя;<br>
                            - е-mail;<br>
                            - текста задачи;<br>
                            <br>
                            Стартовая страница - список задач с возможностью сортировки по имени пользователя, email и
                            статусу. Вывод задач нужно сделать страницами по 3 штуки (с пагинацией). Видеть список задач
                            и создавать новые может любой посетитель без авторизации.<br>
                            <br>
                            Сделайте вход для администратора (логин "admin", пароль "123"). Администратор имеет
                            возможность редактировать текст задачи и поставить галочку о выполнении. Выполненные задачи
                            в общем списке выводятся с соответствующей отметкой "отредактировано администратором".<br>
                            <br>
                            В приложении нужно с помощью чистого PHP реализовать модель MVC. Фреймворки PHP использовать
                            нельзя, библиотеки - можно. Этому приложению не нужна сложная архитектура, решите
                            поставленные задачи минимально необходимым количеством кода. Верстка на bootstrap, к дизайну
                            особых требований нет.</p>
                    </div>
                    <div class="col-sm-4 offset-md-1 py-4">
                        <h4 class="text-white">Profile</h4>
                        <ul class="list-unstyled">
                            <?php if (Auth::isLogined()) {?>
                            <li><a href="logout" class="text-white">LOG OUT</a></li>
                            <?php } else {?>
                            <li><a href="login" class="text-white">LOG IN</a></li>
                            <?php }?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar navbar-dark bg-dark shadow-sm">
            <div class="container d-flex justify-content-between">
                <a href="/beejee" class="navbar-brand d-flex align-items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" stroke="currentColor"
                        stroke-linecap="round" stroke-linejoin="round" stroke-width="2" aria-hidden="true" class="mr-2"
                        viewBox="0 0 24 24" focusable="false">
                        <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z" />
                        <circle cx="12" cy="13" r="4" /></svg>
                    <strong>Tasks</strong>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader"
                    aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <?php if (Auth::isLogined()) {?>
                <a href="logout" class="text-white">Выйти (LOG OUT)</a>
                <?php } else {?>
                <a href="login" class="text-white">Войти (LOG IN)</a>
                <?php }?>
            </div>
        </div>
    </header>
    <div class="container message-container"> </div>
    <script>
    function getCookie(name) {
        let matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    function setCookie(name, value, options = {}) {
        options = {
            path: '/',
            // при необходимости добавьте другие значения по умолчанию
            ...options
        };
        if (options.expires instanceof Date) {
            options.expires = options.expires.toUTCString();
        }
        let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);
        for (let optionKey in options) {
            updatedCookie += "; " + optionKey;
            let optionValue = options[optionKey];
            if (optionValue !== true) {
                updatedCookie += "=" + optionValue;
            }
        }
        document.cookie = updatedCookie;
    }

    function deleteCookie(name) {
        setCookie(name, "", {
            'max-age': -1
        })
    }
    function showMessage(newMessage) {
        if (newMessage !== undefined) {
            let messageContainer = document.querySelector(".message-container");
            let h4 = document.createElement("h4");
            let messageBadge = document.createElement("span");
            messageBadge.classList.add("badge");
            messageBadge.classList.add("badge-success");
            let re = /\+/gi;
            var newstr = newMessage.replace(re, ' ');
            messageBadge.innerText = newstr;
            h4.appendChild(messageBadge)
            messageContainer.appendChild(h4);
            setTimeout(() => {
                messageContainer.innerText = ""
            }, 3000)
        }
    }
    let resultMessage = getCookie('operation_result');
    showMessage(resultMessage)
    deleteCookie('operation_result');
    </script>