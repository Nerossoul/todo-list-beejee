<?php require_once "views/template/heared.php"?>
<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

?>
<div class="container mt-3">
    <form action="create" method="POST">
        <div class="form-group">
            <label for="exampleInputEmail1">Имя пользователя</label>
            <input type="text" name="user_name" class="form-control" id="InputUserName" aria-describedby="nameHelp"
                required>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">E-mail</label>
            <input type="email" name="e_mail" class="form-control" id="InputEmail" aria-describedby="emailHelp"
                required>
        </div>
        <div class="form-group">
            <label for="FormControlTextarea1">Текст задачи</label>
            <textarea class="form-control" name="description" id="FormControlTextarea1" rows="2" required></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Записать</button>
    </form><br>
</div>
<main role="main">
    <div class="container">
        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col"><a
                                href="?page=<?php echo $viewData['page']; ?>&orderBy=user_name&desc=<?php echo $viewData['desc']; ?>">Имя
                                пользователя</a></th>
                        <th scope="col"><a
                                href="?page=<?php echo $viewData['page']; ?>&orderBy=e_mail&desc=<?php echo $viewData['desc']; ?>">E-mail</a>
                        </th>
                        <th scope="col">Текст задачи</th>
                        <th scope="col"><a
                                href="?page=<?php echo $viewData['page']; ?>&orderBy=done&desc=<?php echo $viewData['desc']; ?>">Статус</a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($viewData["tasks"] as $task) {?>
                    <tr>
                        <td><?php echo $task["user_name"] ?></td>
                        <td><?php echo $task["e_mail"] ?></td>
                        <td>

                            <?php
if (Auth::isLogined()) {?>
                            <form action="taskdescription" method="POST">
                                <input type="number" name="id" class="form-control" value="<?php echo $task['id'] ?>"
                                    required hidden>
                                <div class="form-group">
                                    <textarea class="form-control" name="description" rows="2"
                                        required><?php echo $task["description"]; ?></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm">Сохранить</button>
                            </form>
                            <?php } else {echo $task["description"];
}
    ?>
                            <?php if ($task["isAdminEdit"]) {?>
                            <br><span class="badge badge-danger">отредактировано администратором</span>
                            <?php }?>
                        </td>
                        <td>

                            <?php
$toggleDone = "#";

    if (Auth::isLogined()) {
        $toggleDone = "taskdone?id=" . $task['id'] . "&page=" . $viewData['page'];
        if ($task["done"]) {
            $toggleDone .= "&done=0";
        } else {
            $toggleDone .= "&done=1";
        }
    }
    if ($task["done"]) {?>
                            <a class="btn btn-primary btn-sm" href="<?php echo $toggleDone ?>" role="button">выполнено</a>
                            <?php } else {?>
                            <a class="btn btn-secondary btn-sm" href="<?php echo $toggleDone ?>" role="button">не готово</a>
                            <?php }?>
                        </td>
                    </tr>
                    <?php }?>
                </tbody>
            </table>
            <?php
if ($viewData['pages'] > 1) {
    for ($page = 1; $page <= $viewData['pages']; $page += 1) {
        if ($page == $viewData['page']) {
            $btnClass = "btn-primary";
        } else {
            $btnClass = "btn-secondary";
        }
        ?>

            <a class="btn <?php echo $btnClass; ?>"
                href="?page=<?php echo $page; ?>&orderBy=<?php echo $viewData['orderBy']; ?>&desc=<?php echo !$viewData['desc']; ?>"
                role="button"><?php echo $page; ?></a>
            <?php }
}?>
        </div>
    </div>
</main>
<?php require_once "views/template/footer.php"?>