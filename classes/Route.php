<?php
class Route
{
    public static $validRoutes = array();
    public static $currentRoute = "";
    public static function set($route, $handler)
    {
        self::$validRoutes[] = $route;
        if ($_GET["url"] === $route) {
            self::$currentRoute = $_GET["url"];
            $handler->__invoke();
        }
    }
}