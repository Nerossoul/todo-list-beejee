<?php
class DataBase
{
    public static $host = "localhost";
    public static $dbName = "srv61453_beejee";
    public static $username = "srv61453_beejee";
    public static $password = "beejee";
    public static $statement = false;
    private static function connect()
    {
        $pdo = new PDO("mysql:host=" . self::$host . ";dbname=" . self::$dbName . ";charset=utf8;", self::$username, self::$password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        self::$statement = $pdo;
    }
    public static function query($query, $params = array())
    {
        self::connect();
        $statement = self::$statement->prepare($query);
        $statement->execute($params);

        if (explode(" ", $query)[0] == 'SELECT') {
            $data = $statement->fetchAll();
            return $data;
        }
    }

    public static function escape($string)
    {
        if (self::$statement === false) {
            self::connect();
        }
        return self::$statement->quote($string);
    }

}